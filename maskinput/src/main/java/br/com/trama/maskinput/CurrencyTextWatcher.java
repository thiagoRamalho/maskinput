package br.com.trama.maskinput;

import java.text.NumberFormat;
import java.util.Locale;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;

public class CurrencyTextWatcher implements TextWatcher{

	boolean isUpdating;
	private StringBuilder valueFinal;
	private boolean mSelfChange;
	private String regExRawValue = "[[^0-9]]";
	private Locale locale;

	public CurrencyTextWatcher() {
		this(Locale.getDefault());
		this.valueFinal = new StringBuilder("");
	}
	
	public CurrencyTextWatcher(Locale locale) {
		super();
		this.locale = locale;
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {

		if(isUpdating){
			isUpdating = false;
			return;
		}

		String value = s.toString().replaceAll(this.regExRawValue, "");

		valueFinal = new StringBuilder("");
		
		//clean the component (ex.: EditText)
		if(value.length() < 1){
			return;
		}

		NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
		
		try{
			
			value = nf.format(Double.parseDouble(value)/100);
			
			valueFinal.append(value);
			
		}catch(NumberFormatException e){
			valueFinal = new StringBuilder("");
		}
		
		isUpdating = true;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void afterTextChanged(Editable s) {

		if (mSelfChange) {
			// Ignore the change caused by s.replace().
			return;
		}
		
		//replace Editable element with mask value
		mSelfChange = true;
		s.replace(0, s.length(), this.valueFinal);
		mSelfChange = false;

		//bind value in component
		Selection.setSelection(s, s.length());	
	}
	
	public String getMaskedValue(){
		return this.valueFinal.toString();
	}
}
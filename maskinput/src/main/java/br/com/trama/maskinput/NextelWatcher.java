package br.com.trama.maskinput;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;

public class NextelWatcher implements TextWatcher{

	boolean isUpdating;
	private StringBuilder valueFinal;
	private boolean mSelfChange;
	private String regExRawValue = "[^[0-9*]]";

	public void onTextChanged(CharSequence s, int start, int before, int count) {

		if(isUpdating){
			isUpdating = false;
			return;
		}

		String value = s.toString().replaceAll(this.regExRawValue, "");

		valueFinal = new StringBuilder("");
		
		//clean the component (ex.: EditText)
		if(value.length() < 1){
			return;
		}
		
		valueFinal.append(value);
		
		isUpdating = true;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void afterTextChanged(Editable s) {

		if (mSelfChange) {
			// Ignore the change caused by s.replace().
			return;
		}
		
		//replace Editable element with mask value
		mSelfChange = true;
		s.replace(0, s.length(), this.valueFinal);
		mSelfChange = false;

		//bind value in component
		Selection.setSelection(s, s.length());	
	}
}

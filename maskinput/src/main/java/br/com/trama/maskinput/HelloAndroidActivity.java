package br.com.trama.maskinput;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

public class HelloAndroidActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        EditText phoneEdit = (EditText) findViewById(R.id.phoneEdit);
        phoneEdit.addTextChangedListener(new MaskWatcher("(##)####-####", "[[^a-zA-Z0-9]]"));
        
        EditText dateEdit = (EditText) findViewById(R.id.dtEdit);
        dateEdit.addTextChangedListener(new MaskWatcher("##/##/####", "[[^0-9]]"));
        
        EditText nextelEdit = (EditText) findViewById(R.id.nextelEdit);
        nextelEdit.addTextChangedListener(new CurrencyTextWatcher());
    }

}


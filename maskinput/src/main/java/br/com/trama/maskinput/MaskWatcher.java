package br.com.trama.maskinput;

import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;

public class MaskWatcher implements TextWatcher{

	boolean isUpdating;
	private String mask;
	private StringBuilder valueFinal;
	private boolean mSelfChange;
	private String regExRawValue;

	
	public MaskWatcher(String mask, String regExRawValue) {
		this.mask = mask;
		this.regExRawValue = regExRawValue;
		
		if(TextUtils.isEmpty(this.mask)){
			throw new IllegalArgumentException("invalid mask");
		}
		
		if(TextUtils.isEmpty(this.regExRawValue)){
			throw new IllegalArgumentException("invalid regExRawValue");
		}
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {

		if(isUpdating){
			isUpdating = false;
			return;
		}

		String value = s.toString().replaceAll(this.regExRawValue, "");

		valueFinal = new StringBuilder("");
		
		//clean the component (ex.: EditText)
		if(value.length() < 1){
			return;
		}

		int idxChar = 0;

		for(int i = 0; i < mask.length(); i++){

			char maskChar = mask.charAt(i);

			if(maskChar == '#'){
				valueFinal.append(value.charAt(idxChar++));
			}
			else {
				valueFinal.append(maskChar);
			}

			if(idxChar >= value.length()){
				break;
			}
		}

		isUpdating = true;
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void afterTextChanged(Editable s) {

		if (mSelfChange) {
			// Ignore the change caused by s.replace().
			return;
		}
		
		//replace Editable element with mask value
		mSelfChange = true;
		s.replace(0, s.length(), this.valueFinal);
		mSelfChange = false;

		//bind value in component
		Selection.setSelection(s, s.length());	
	}
}